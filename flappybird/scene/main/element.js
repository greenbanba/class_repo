// this file contains some elements in this game
class BaseElement {
    constructor(game, name) {
        this.game = game
        log(this.game)
        this.texture = this.game.imageByName(name)
        this.x = 0
        this.y = 0
        this.w = this.texture.width
        this.h = this.texture.height
        this.cooldown = 0
    }
    static new(game, name) {
        var i = new this(game, name)
        return i
    }
    draw() {
        this.game.drawGuaimg(this)
    }
    update() { }
}

class Background extends BaseElement {
    constructor(game, name) {
        super(game, name)
        this.y = 300
        this.h = 200
        this.w = 400
    }
}


class Pipe extends BaseElement {
    constructor(game, name) {
        super(game, name)
        this.rotate = 0
        this.flipY = true
    }
}

class Pipes {
    constructor(game) {
        this.game = game
        this.pipes = []
        this.columnPipes = 3
        this.pipeHorizontalSpace = 130
        this.pipeVerticalSpace = 200
        for (let i = 0; i < this.columnPipes; i++) {
            let down = Pipe.new(game, 'pipe')
            down.flipY = false
            down.x = 500 + i * this.pipeVerticalSpace
            let up = Pipe.new(game, 'pipe')
            up.x = down.x
            this.resetPipesPosition(up, down)
            this.pipes.push(up)
            this.pipes.push(down)
        }
    }
    static new(game) {
        return new this(game)
    }
    update() {
        for (let index = 0; index < this.pipes.length; index++) {
            let p = this.pipes[index]
            p.x -= 5
            if (p.x < -100) {
                p.x += this.pipeVerticalSpace * this.columnPipes
            }
        }
    }

    draw() {
        let context = this.game.context
        for (let index = 0; index < this.pipes.length; index++) {
            context.save()

            let p = this.pipes[index]

            let w2 = p.w / 2
            let h2 = p.h / 2
            context.translate(p.x + w2, p.y + h2)

            let scaleX = p.flipX ? -1 : 1
            let scaleY = p.flipY ? -1 : 1
            context.scale(scaleX, scaleY)

            context.rotate(p.rotation * Math.PI / 180)
            context.translate(-w2, -h2)
            context.drawImage(p.texture, 0, 0)
            context.restore()
        }
    }
    resetPipesPosition(up, down) {
        up.y = randomRange(-200, 0)
        down.y = up.y + up.h + this.pipeHorizontalSpace
    }
}

class BaseAnimation {
    // abstract class for all animation
    constructor(game) {
        this.game = game
        // frames
        this.frames = []
        this.initFrame()
        this.texture = this.frames[0]
        this.frameIndex = 0
        this.frameCount = 3

        // texture info 
        this.x = 100
        this.y = 340
        this.w = this.texture.width
        this.h = this.texture.height
    }
    static new(game) {
        var i = new this(game)
        return i
    }
    draw() {
        this.game.drawGuaimg(this)
    }
    update() { }
}

class Bird extends BaseAnimation {
    constructor(game) {
        super(game)
        this.x = 180
        this.vy = 0
        this.gy = 9.8
        this.rotation = 0
        this.idle = false
    }
    initFrame() {
        for (let index = 1; index < 5; index++) {
            let name = `bird${index}`
            let b = this.game.imageByName(name)
            this.frames.push(b)
        }
    }

    jump() {
        this.vy = -10
        this.rotation = -45
    }
    update() {

        // gravatitiy
        this.y += this.vy
        this.vy += this.gy * 0.2

        // if idle just fly no move
        if (this.idle) {
            this.vy = 0
            this.gy = 0
            this.rotation = 0
        }


        // stick on ground
        let low = 480
        if (this.y > low) {
            this.y = low
        }
        // stick on sky
        let high = 10
        if (this.y < high) {
            this.y = high
        }

        // rotatate the head
        if (this.rotation < 45) {
            this.rotation += 5
        }

        // change the texture frame
        this.frameCount--
        if (this.frameCount == 0) {
            this.frameCount = 3
            this.frameIndex = (this.frameIndex + 1) % this.frames.length
            this.texture = this.frames[this.frameIndex]
        }
    }
    move(x, keyStatus) {
        this.flipX = (x > 0)
        this.x += x
    }
    // TODO:implement me!
    draw() {
        let context = this.game.context
        context.save()
        let w2 = this.w / 2
        let h2 = this.h / 2
        context.translate(this.x + w2, this.y + h2)
        context.rotate(this.rotation * Math.PI / 180)
        context.translate(-w2, -h2)
        context.drawImage(this.texture, 0, 0)
        context.restore()
    }
}
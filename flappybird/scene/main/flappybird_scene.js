class FlappyBird extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }
    setup() {
        this.setupElements()
        this.setupEvents()
        this.score = 0
    }
    setupEvents() {
        this.game.registerAction(
            'w',
            () => {
                this.bird.jump()
            }
        )
    }
    setupElements() {
        // order matters!!
        // set up background
        let bg = Background.new(this.game, 'background')
        this.addElement(bg)

        // set up pipes
        this.pipes = Pipes.new(this.game)
        this.addElement(this.pipes)

        // set up ground
        this.skipCount = 4
        this.grounds = []
        for (let i = 0; i < 30; i++) {
            let g = BaseElement.new(this.game, 'ground')
            g.x = i * 19
            g.y = 500
            this.addElement(g)
            this.grounds.push(g)
        }

        // set up bird
        this.bird = Bird.new(this.game)
        this.addElement(this.bird)
    }
    update() {
        super.update()
        this.skipCount--
        let offset = -5
        if (this.skipCount == 0) {
            this.skipCount = 4
            offset = 15
        }
        for (let index = 0; index < 30; index++) {
            let g = this.grounds[index]
            g.x += offset
        }
        // change score if not dead
        this.score += 1 / 30

        this.die()
    }
    die() {
        for (let index = 0; index < this.pipes.pipes.length; index++) {
            let pipe = this.pipes.pipes[index]
            if (rectIntersects(pipe, this.bird)) {
                let s = SceneEnd.new(this.game)
                s.score = this.score
                this.game.replaceScene(s)
            }
        }
    }
    draw() {
        super.draw()
        this.game.context.fillText(`坚持时长 : ${this.score.toFixed(2)} 秒`, 10, 20)
    }
}
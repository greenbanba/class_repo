class SceneEnd extends GuaScene {
    constructor(game, score) {
        super(game)
        game.registerAction('r', function () {
            var s = SceneTitle.new(game)
            game.replaceScene(s)
        })
    }
    draw() {
        // draw labels
        this.game.context.fillText(`恭喜你，这次坚持了${this.score.toFixed(2)} 秒, 按 r 返回标题界面`, 100, 290)
    }
}

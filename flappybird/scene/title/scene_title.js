class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }
    setup() {
        this.setupElements()
        this.setupEvents()
    }
    setupEvents() {
        this.game.registerAction(
            'w',
            () => {
                var s = FlappyBird.new(this.game)
                this.game.replaceScene(s)
            }
        )
    }
    setupElements() {
        // order matters!!
        // set up background
        let bg = Background.new(this.game, 'background')
        this.addElement(bg)

        // set up ground
        this.skipCount = 4
        this.grounds = []
        for (let i = 0; i < 30; i++) {
            let g = BaseElement.new(this.game, 'ground')
            g.x = i * 19
            g.y = 500
            this.addElement(g)
            this.grounds.push(g)
        }

        // set up bird
        this.bird = Bird.new(this.game)
        this.bird.idle = true
        this.addElement(this.bird)
    }
    update() {
        super.update()
        this.skipCount--
        let offset = -5
        if (this.skipCount == 0) {
            this.skipCount = 4
            offset = 15
        }
        for (let index = 0; index < 30; index++) {
            let g = this.grounds[index]
            g.x += offset
        }
    }
    draw() {
        // draw labels
        super.draw()
        this.game.context.fillText('按下w开始游戏', 150, 200)
    }

}
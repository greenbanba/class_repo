var loadLevel = function (game, n) {
    n = n - 1
    var level = levels[n]
    var blocks = []
    for (var i = 0; i < level.length; i++) {
        var p = level[i]
        var b = Block(game, p)
        blocks.push(b)
    }
    return blocks
}

var enableDebugMode = function (game, enable) {
    if (!enable) {
        return
    }
    window.paused = false
    window.addEventListener('keydown', function (event) {
        var k = event.key
        if (k == 'p') {
            // 暂停功能
            window.paused = !window.paused
        } else if ('1234567'.includes(k)) {
            // blocks = loadLevel(game, Number(k))
        }
    })
}

var __main = function () {
    var images = {
        background: 'img/sky.png',
        bird1: 'img/bird-01.png',
        bird2: 'img/bird-02.png',
        bird3: 'img/bird-03.png',
        bird4: 'img/bird-04.png',
        ground: 'img/ground.png',
        pipe: 'img/pipe.png',
    }

    var game = GuaGame.instance(30, images, function (g) {
        var s = SceneTitle.new(g)
        // var s = PlaneScene.new(g)
        // let s = FlappyBird.new(g)
        // let s = SceneEnd.new(g)


        // var s = ParticleScene.new(g)
        log('start scene', s)
        g.runWithScene(s)
    })

    enableDebugMode(game, true)
}

__main()